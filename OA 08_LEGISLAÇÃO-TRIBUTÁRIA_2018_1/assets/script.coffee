###
	       OA TEMPLATE 4.1
	----------------------------
	Desenvolvido em CoffeeScript

		Código: Fabiane Lima
###

# ----- Pré-carregamento das imagens ----- #
imgs =	[
	'assets/img/img1.jpg'
	'assets/img/img2.jpg'
	'assets/img/img3.jpg'
	'assets/img/img4.jpg'
]

preload = (imgs) ->
	counter = 0

	$(imgs).each ->
		$('<img />').attr('src', this).appendTo('body').css { display: 'none' }
		counter++

	if counter is imgs.length
		$('main').css { opacity: '1' }
		$('body').css { background: '#e7e7e7' }

$(window).on 'load', -> preload(imgs)


# ----- Funções e dados ----- #
$ ->
	ctrl = 0
	data =	[]
	func =
		end: ->
			$('.dimmer').delay(300).fadeIn()
			$('.modal').html('<h1>Finalizando...</h1><p>Neste infográfico, retomamos o conteúdo de responsabilidade tributária, e aprendemos que a responsabilidade tributária, quando analisada na sua concepção mais ampla, traduz-se na submissão de determinada pessoa ao poder do Estado de impor a tributação.</p><button class="again">Ver novamente</button>')

		dismiss: -> $('.dimmer').fadeOut()

		nxt: ->
			ctrl++
			if ctrl isnt 4 then $('.slides').animate { top: '-=100%' }
			else func.end()


# ----- Eventos ----- #
	$(document).on 'click', '.nxt', -> func.nxt()
	$(document).on 'click', '.help', -> func.help()
	$(document).on 'click', '.info', -> func.info()
	$(document).on 'click', '.end', -> func.end()
	$(document).on 'click', '.dismiss', -> func.dismiss()
	$(document).on 'click', '.again', -> location.reload()
