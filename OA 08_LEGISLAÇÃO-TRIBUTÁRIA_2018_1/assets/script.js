
/*
	       OA TEMPLATE 4.1
	----------------------------
	Desenvolvido em CoffeeScript

		Código: Fabiane Lima
 */

(function() {
  var imgs, preload;

  imgs = ['assets/img/img1.jpg', 'assets/img/img2.jpg', 'assets/img/img3.jpg', 'assets/img/img4.jpg'];

  preload = function(imgs) {
    var counter;
    counter = 0;
    $(imgs).each(function() {
      $('<img />').attr('src', this).appendTo('body').css({
        display: 'none'
      });
      return counter++;
    });
    if (counter === imgs.length) {
      $('main').css({
        opacity: '1'
      });
      return $('body').css({
        background: '#e7e7e7'
      });
    }
  };

  $(window).on('load', function() {
    return preload(imgs);
  });

  $(function() {
    var ctrl, data, func;
    ctrl = 0;
    data = [];
    func = {
      end: function() {
        $('.dimmer').delay(300).fadeIn();
        return $('.modal').html('<h1>Finalizando...</h1><p>Neste infográfico, retomamos o conteúdo de responsabilidade tributária, e aprendemos que a responsabilidade tributária, quando analisada na sua concepção mais ampla, traduz-se na submissão de determinada pessoa ao poder do Estado de impor a tributação.</p><button class="again">Ver novamente</button>');
      },
      dismiss: function() {
        return $('.dimmer').fadeOut();
      },
      nxt: function() {
        ctrl++;
        if (ctrl !== 4) {
          return $('.slides').animate({
            top: '-=100%'
          });
        } else {
          return func.end();
        }
      }
    };
    $(document).on('click', '.nxt', function() {
      return func.nxt();
    });
    $(document).on('click', '.help', function() {
      return func.help();
    });
    $(document).on('click', '.info', function() {
      return func.info();
    });
    $(document).on('click', '.end', function() {
      return func.end();
    });
    $(document).on('click', '.dismiss', function() {
      return func.dismiss();
    });
    return $(document).on('click', '.again', function() {
      return location.reload();
    });
  });

}).call(this);
